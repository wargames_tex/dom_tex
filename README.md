# The Drive on Metz

[[_TOC_]]

This is my remake of the wargame _The Drive on Metz_.  The original
game was published in [James F. Dunnigan's _The Complete
  Wargamer Handbook_][], in 1980.


## Game mechanics 

|              |                    |
|--------------|--------------------|
| Period       | WWII               |
| Level        | Operational        |
| Hex scale    | 4km (2.5miles)     |
| Unit scale   | regiment (lll)     |
| Turn scale   | 1 day              |
| Unit density | low                |
| # of turns   | 7                  |
| Complexity   | 1 of 10            |
| Solitaire    | 9 of 10            |

Features:
- Introductory
- Optional rules

## About this rework 

This rework is entirely new.  All text and graphics is new and nothing
is copied verbatim from the original materials. 

I have restructured and rewritten the rules somewhat (the text is all
new), as well as added illustrations of the rules. 


## The files 

The distribution consists of the following files 

- [DriveOnMetzA4.pdf][] This is a single document that contains
  everything: The rules, charts, counters, and the board.
  
  This document is meant to be printed on a duplex A4 printer.  The
  charts, counters, and board will be on separate sheets and can be
  separated from the rules.
  
- [DriveOnMetzA4Booklet.pdf][] This document _only_ contains the
  rules.  It is meant to be printed on a duplex A4 printer with long
  edge binding.  This will produce 3 sheets which should be folded
  down the middle of the long edge and stabled to form an 12-page A5
  booklet of the rules.
  
- [materialsA4.pdf][] holds the charts, counters, and board.  It is
  meant to be printed on A4 paper.  Print and glue on to a relatively
  thick piece of cardboard (1.5mm or so) or the like.
  
- [scoresheetA4.pdf][] is a set of sheets to record victory points. 
  
    
If you only have access to US Letter printer, you can use the
following files

| *A4 Series*                  | *Letter Series*                  |
| -----------------------------|----------------------------------|
| [DriveOnMetzA4.pdf][]        | [DriveOnMetzLetter.pdf][]  	  |
| [DriveOnMetzA4Booklet.pdf][] | [DriveOnMetzLetterBooklet.pdf][] |
| [materialsA4.pdf][]	       | [materialsLetter.pdf][]	      |
| [scoresheetA4.pdf][]	       | [scoresheetLetter.pdf][]	      |

Download [artifacts.zip][] to get all files for both kinds of paper
formats. 

## VASSAL module

Also available is a [VASSAL](https://vassalengine.org) module

- [VMOD][DriveOnMetz.vmod]

The module is generated from the same sources as the Print'n'Play
documents, and contains the rules as an embedded PDF.  The modules
features

- Battle markers 
- Automatic odds markers 
- Automatic battle resolution
- Automatic victory point calculations
- Optional rules selection
- Embedded rules 
- Moved and battle markers automatically cleared 
- Symbolic dice 
- Separate layers for units and markers 
- Tutorial based Chapter 1 of [James F. Dunnigan's _The Complete
  Wargamer Handbook_][]

## Previews

![Board](.imgs/hexes.png)
![Charts](.imgs/tables.png)
![Counters](.imgs/chits.png)
![Front](.imgs/front.png)
![VASSAL module](.imgs/vassal.png)
![Components](.imgs/photo_components.png)
![Turn 1](.imgs/photo_turn1.png)

## Implementation 

The whole package is implemented in LaTeX using my package
[_wargame_](https://gitlab.com/wargames_tex/wargame_tex).  This
package, combined with the power of LaTeX, produces high-quality
documents, with vector graphics to ensure that everything scales.

## Other version of the game

This implements the _original_ game as published in 

[James F. Dunnigan's _The Complete Wargamer Handbook_][], in 1980.
Victory Point Games (VPG) later, in 2008, published 
[The Drive on Metz - Patton in France, September 1944](https://web.archive.org/web/20131207060218/http://victorypointgames.com/details.php?prodId=61).
The changes in the second edition are 

- Some map changes
  - Hex 0707 is not fortified, but hex 0706 is.
  - Hex 0707 is not connected to 0708 via road, but connects directly
	to 0607. 
  - There is no road leading off-map in 0906, but there is one 0902.
- Additional optional rules.
  - US 90/359 IREGT arrive on turn 4.
  - One, max two, improved German units (+1 to CF).
- Different victory points
  - US Faction
    - 1 VP per unit starting its turn east of Moselle
    - 5 VP for control of Thionville (0701)
    - 20 VP for control of Metz (0807)
    - 5 VP for each unit exitting over East edge
    - Optionally, variable VPs for each improved German unit.
  - German Faction
    - 10 VP for each unit exitting over west edge
    - (8-turn) VP for each panzergrenadier unit exitting over east
       or south edge.  The exact number of VPs depend on the turn on
       which the unit exits the map
    - 1 VP at end of German combat phase if no US units east of
       Moselle.
    - 1 VP per US CF eliminated.
    - Optionally, 5 VP if the US 101/502 ABIREGT unit enters the
       map.
    - Optionally, 3 VP if the US 90/359 IREGT unit enters the
       map.


[artifacts.zip]: https://gitlab.com/wargames_tex/dom_tex/-/jobs/artifacts/master/download?job=dist

[DriveOnMetzA4.pdf]: https://gitlab.com/wargames_tex/dom_tex/-/jobs/artifacts/master/file/DriveOnMetz-A4-master/DriveOnMetzA4.pdf?job=dist
[DriveOnMetzA4Booklet.pdf]: https://gitlab.com/wargames_tex/dom_tex/-/jobs/artifacts/master/file/DriveOnMetz-A4-master/DriveOnMetzA4Booklet.pdf?job=dist
[materialsA4.pdf]: https://gitlab.com/wargames_tex/dom_tex/-/jobs/artifacts/master/file/DriveOnMetz-A4-master/materialsA4.pdf?job=dist
[scoresheetA4.pdf]: https://gitlab.com/wargames_tex/dom_tex/-/jobs/artifacts/master/file/DriveOnMetz-A4-master/scoresheetA4.pdf?job=dist
[DriveOnMetz.vmod]: https://gitlab.com/wargames_tex/dom_tex/-/jobs/artifacts/master/file/DriveOnMetz-A4-master/DriveOnMetz.vmod?job=dist


[DriveOnMetzLetter.pdf]: https://gitlab.com/wargames_tex/dom_tex/-/jobs/artifacts/master/file/DriveOnMetz-Letter-master/DriveOnMetzLetter.pdf?job=dist
[DriveOnMetzLetterBooklet.pdf]: https://gitlab.com/wargames_tex/dom_tex/-/jobs/artifacts/master/file/DriveOnMetz-Letter-master/DriveOnMetzLetterBooklet.pdf?job=dist
[materialsLetter.pdf]: https://gitlab.com/wargames_tex/dom_tex/-/jobs/artifacts/master/file/DriveOnMetz-Letter-master/materialsLetter.pdf?job=dist
[scoresheetLetter.pdf]: https://gitlab.com/wargames_tex/dom_tex/-/jobs/artifacts/master/file/DriveOnMetz-Letter-master/scoresheetLetter.pdf?job=dist

[James F. Dunnigan's _The Complete Wargamer Handbook_]: https://archive.org/details/CompleteWargamesHandbookDunnigan/
