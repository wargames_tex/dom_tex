#
#
#
NAME		:= DriveOnMetz
VERSION		:= 1.0-1
VMOD_VERSION	:= 1.1.0
VMOD_TITLE      := The Drive on Metz

LATEX		:= lualatex
DOCFILES	:= $(NAME).tex			\
		   front.tex			\
		   preface.tex			\
		   rules.tex			\
		   materials.tex		
DATAFILES	:= hexes.tex			\
		   chits.tex			\
		   tables.tex			\
		   score.tex
IMGFILES	:= hexes.pdf			\
		   tables.pdf			\
		   chits.pdf			\
		   score.pdf
BOARD		:= hexes.pdf
OTHER		:= 
STYFILES	:= dom.sty			\
		   commonwg.sty
SIGNATURE	:= 12
PAPER		:= --a4paper
TARGETSA4	:= $(NAME)A4.pdf 		\
		   $(NAME)A4Booklet.pdf 	\
		   materialsA4.pdf		\
		   scoresheetA4.pdf

TARGETSLETTER	:= $(NAME)Letter.pdf 		\
		   $(NAME)LetterBooklet.pdf 	\
		   materialsLetter.pdf		\
		   scoresheetLetter.pdf
TUTORIAL	:= Tutorial.vlog

include Variables.mk
include Patterns.mk

all:	a4
a4:	$(TARGETSA4) vmod
us:	$(TARGETSLETTER)
letter:	us
vmod:	$(NAME).vmod
mine:	a4 cover.pdf

include Rules.mk

.imgs/chits.png:chits.png
	$(CONVERT) $< $(CONVERT_FLAGS) $@

.imgs/hexes.png:hexes.png
	$(CONVERT) $< $(CONVERT_FLAGS) $@

.imgs/tables.png:tables.png
	$(CONVERT) $< $(CONVERT_FLAGS) $@

.imgs/front.png:front.png
	$(CONVERT) $< $(CONVERT_FLAGS) $@

.imgs/logo.png:logo.png
	mv $< $@


update-images:	.imgs/chits.png 	\
		.imgs/hexes.png 	\
		.imgs/tables.png	\
		.imgs/front.png		\
		.imgs/logo.png		

# These generate images that are common for all formats 
hexes.pdf:		hexes.tex	$(STYFILES)
chits.pdf:		chits.tex	$(STYFILES)
tables.pdf:		tables.tex	$(STYFILES)
frontA4.pdf:		front.tex       $(STYFILES)
export.pdf:		export.tex	$(DATAFILES)	$(STYFILES)
cover.pdf:		cover.tex	tables.tex hexes.pdf $(STYFILES)

$(NAME).pdf:		$(NAME).aux
$(NAME).aux:		$(DOCFILES) 	$(STYFILES)	$(IMGFILES)
materials.pdf:		materials.tex	$(STYFILES)	$(IMGFILES)

$(NAME)A4.pdf:		$(NAME)A4.aux
$(NAME)A4.aux:		$(DOCFILES) 	$(STYFILES) 	$(IMGFILES)
materialsA4.pdf:	materials.tex 	$(STYFILES) 	$(IMGFILES)
frontA4.pdf:		front.tex 	tables.tex hexes.pdf $(STYFILES)

$(NAME)Letter.pdf:	$(NAME)Letter.aux
$(NAME)Letter.aux:	$(DOCFILES) 	$(STYFILES) 	$(IMGFILES)
materialsLetter.pdf:	materials.tex 	$(STYFILES) 	$(IMGFILES)

$(NAME)Rules.pdf:	$(NAME)Rules.aux
$(NAME)Rules.aux:	$(DOCFILES) 	$(STYFILES) 	$(IMGFILES)

# Generic dependency

$(NAME)LetterBooklet.pdf:PAPER=--letterpaper
$(NAME)LetterBooklet.pdf:SIGNATURE=4


include Docker.mk

docker-prep::
	mktextfm frunmn
	mktextfm frunbn

.PRECIOUS:	export.pdf $(NAME)Rules.pdf $(NAME).vtmp

#
# EOF
#

