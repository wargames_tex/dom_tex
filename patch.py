from wgexport import *
from pprint import pprint


moreHelp = '''
<html>
 <head>
  <title>{title}</title>
  <style>
  </style>
 </head>
 <body>
  <h1>{title}</h1>
  <h2>Content</h2>
  <ul>
   <li><a href="#rules>Rules</a></li>
   <li><a href="#setup>Optional rules and start turn</a></li>
   <li><a href="#turn">Turn and phase tracker</a></li>
   <li><a href="#reinf">Reinforcements</a></li>
   <li><a href="#battle">Battle declaration and resolution</a></li>
   <li><a href="#vp">Victory points and conditions</a></li>
   <li><a href="#tut">Tutorial</a></li>
   <li><a href="#about">About this game</a></li>
   <li><a href="#credits">Credits</a></li>
  </ul>
  <h2><a name="rules"></a>Rules</h2>
  <p>
    A PDF of the rules is available through the <b>Help</b> menu.
    Please note that it may take a little while for your PDF viewer
    to launch
  </p>
  <h2><a name="setup"></a>Optional rules</h2>

  <p> At the start of a new game, you will be presented with a window
   to select which optional rules should be in effect.  You will
   <i>not</i> be able to change these settings later in the game.
  </p>

  <p> All units are placed in their setup locations: German units on
   their start hex, except DE 106 ABDE, and all US units, and DE 106
   ABDE, in their set-upt boxes.
  </p>

  <p> When the game is progressed to the first turn <b>US movement</b>
    phase, then game pieces that are not in used are removed and the
    optional rules are locked.  </p>

  <p> I you play by email, then one of you should select the optional
   rules, save the game <i> before</i> moving to the <b>Allied move
   points</b> phase. , and send it off to the other side for
   validation. Only then should you progress to the next phase.
  </p>

  <h2><a name="turn"></a>Turn and phase tracker</h2>

  <p> Use the <b>Turn tracker</b> interface in the menubar.  A number
   of automatic actions are implemented in this module, which heavily
   depend on the use of that interface.  </p>

  <p> You can go to the next phase by pressing the <b>+</b> button, or
   by pressing <code>Alt-T</code>.  This is the <i>only</i> possible
   way of moving the turn marker.</p>

  <p><b>Note</b>: The die-roll pop-up windows may steal focus from the
   main window, which means that key-board short-cuts, such as
   <code>Alt-T</code> are not registered.  Simply click the main
   window to bring that back into focus, and keyboard short cuts will
   be registered again.</p>

  <h2><a name="reinf"></a>Reinforcements</h2>

  <p>Reinforcement units arrive at the indicated turn.  The factions
    must move them on to the board themselves.  </p>

  <p> <img src="us_101_502_air.png"> &nbsp;The optional US 101/502
   ABIREGT may arrive on <i>any</i> US movement phase, in any empty
   hex, including in enemy ZOC, but may not move the turn of arrival.
   It may conduct combat in the US combat phase of the turn of
   arrival.  </p>

  <h2><img src="battle-marker-icon.png" width=24 height=24/><a
   name="battle"></a> &nbsp;Battle declaration and resolution</h2>

  <p>Battles <i>should</i> be declared in a factions <b>movement</b>
    phase.</p>

  <p>The module can automatically calculate odds and resolve combats
    via the use of <i>battle markers</i>.  To place a battle marker,
    select the attacking <i>and</i> defending units and press the
    battle marker button (<img src="battle-marker-icon.png"/>) or
    <code>Ctrl-X</code>.  This will place markers on the invovled
    units that identifiy the battle uniquely.</p>

  <p>If the user preference <b>Calculate Odds on battle
    declaration</b> is enabled (default), then the module will also
    calculate the odds (inluding bonuses and penalties for terrain),
    and place an odds marker (e.g., <img width=24 heigh=24
    src="odds_marker_1.png"/>) on one of the involved units.</p>

  <p>The module takes into account terrain of the defender, whether
   the attackers attack over a river, and, if the optional rule is
   used, if multiple units of the same division participate in the
   attack.</p>

  <p>If the user preference <b>Resolve battle results
    automatically</b> is enabled (default), then in the factions
    <b>Combat</b> phase, you can right click the odds marker and
    select <b>Resolve</b> (or <code>Ctrl-Y</code> or <img
    src="resolve-battles-icon.png" width=24 height=24> button) to
    resolve the combat.  The module will roll the dice and do the
    appropriate calculations, and a replace the odds marker with
    a <i>result marker</i> (e.g. <img width=24 height=24
    src="result_marker_D1.png"/>, for example).</p>

  <p>The module will <i>not</i> apply the result to the invovled
    units. This <i>must</i> be done, <i>according to the rules</i>, by
    the factions.  If a unit is eliminated as a consequence of the
    combat, then the short cut <code>Ctrl-E</code> or eliminate button
    <img src="eliminate-icon.png" width=24 height=24> can be used.
    The unit is then moved to the factions dead unit pool
    (<code>Alt-E</code>).</p>

  <p>To clear a battle declaration, select one of the involved units
    or battle markers, and select <b>Clear</b> (or press
    <code>Ctrl-C</code>) from the context menu. All battles can be
    cleared by the <img src="clear-battles-icon.png"/> button in the
    menubar.</p>

  <h2><a name="vp"></a>Victory points and conditions</h2>

  <p>The module automatically keeps track of victory points, <i>except</i>
    that factions <i>must</i> adjust the control of two cities: Metz (0807) and Thionville (0801), by using the control marker (<img
    src="control.png" width=24 height=24>, flipped <img
    src="control-flipped.png" width=24 height=24>).

  <p>Who has control of the two cities is recorded by flipping the
    control marker. (<img src="control.png" width=24 height=24>,
    flipped <img src="control-flipped.png" width=24
    height=24>). Select the marker and press <code>Ctrl-F</code>, or
    right-click and select <i>Flip</i> form the context menu.  You may
    need to move any unit in the hex to access the control marker.</p>

  <p>To obtain VPs from a US or German unit moved over the East or
   West edge, respectively, the faction must move the unit
   &quot;offboard&quot; - i.e, to a place on the board that is not a
   hex, nor a reinforcement slot. Pay attention to the chat messages.</p>

  <p>German panzergrenadier and armoured units are considered
   retreated by moving them to the east or south edge of the board.
   Again, pay attention to the chat messages.</p>

  <p><i>Do not</i> use the dead pool (<code>Alt-E</code>) for units
   that left the map voluntarily.  Those will <i>not</i> count towards
   a factions VPs.</p>

  <p>You can press <code>Alt-V</code> (or the &quot;VP&quot; button in
   the tool bar) to see the current VPs.  Note that VPs are <i>only</i>
   calculated at the start of each turn.</p>

  <p>On turn "8", a pop-up will show the final victory points of each
   faction and declare the winner.</p>

  <h2><a name="tut"></a>Tutorial</h2>

  <p>The tutorial of this module is a replay of the first two turns as
   reproduced in the James F. Dunnigan's book <i>The Complete Wargame
   Handbook</i>.  More commentary on the strategic and tactical
   considerations can be found there.  The 2nd edition of the book can
   be found at </p>

  <center>
   <code>https://archive.org/details/CompleteWargamesHandbookDunnigan</code>
  </center>

  <p>To step through the tutorial, press <code>Page-down</code>. 
    On some steps you may need to press the key twice.</p>

  <h2><a name="about"></a>About this game</h2>

  <p> This VASSAL module was created from L<sup>A</sup>T<sub>E</sub>X
   sources of a Print'n'Play version of the <b>The Drive on Metz</b>
   game.  That (a PDF) can be found at </p>

  <center>
    <code>https://gitlab.com/wargames_tex/dom_tex</code>
  </center>

  <p> where this module, and the sources, can also be found.  The PDF
   can also be found as the rules of this game, available in the
   <b>Help</b> menu.  </p>

  <p>This module implements the <i>original</i> game as published in
   James F. Dunnigan's book <i>The Complete Wargame Handbook</i>.</p>

 <p>Victory Point Games (VPG) later reimplemented the game as <i>The
  Drive on Metz - second edition</i> (or <i>The Drive on Metz - Patton
  in France: September, 1944</i>).  The VPG (2nd edition) has the
  following changes.

 <ul>
  <li>Map changes</li>
   <ul>
    <li>Hex 0707 is not fortified, but hex 0706 is.</li>
    <li>Hex 0707 is not connected to 0708 via road, but connects
      directly to 0607.</li> 
    <li>There is no road leading off-map in 0906, but there is one
      0902.</li>
   </ul>
  <li>Additional optional rules.
   <ul>
    <li>US 90/359 IREGT arrive on turn 4.</li>
    <li>One, max two, improved German units (+1 to CF).</li>
   </ul>
  <li>Different Victory Points
   <dl>
    <dt>US Faction</dt>
    <dd>
     <ul>
      <li>1 VP per unit starting its turn east of Moselle</li>
      <li>5 VP for control of Thionville (0701)</li>
      <li>20 VP for control of Metz (0807)</li>
      <li>5 VP for each unit exitting over East edge</li>
      <li>Optionally, variable VPs for each improved German unit.</li>
     </ul>
    </dd>
    <dt>German Faction</dt>
    <dd>
     <ul>
      <li>10 VP for each unit exitting over west edge</li>
      <li>(8-turn) VP for each panzergrenadier unit exitting over east
       or south edge.  The exact number of VPs depend on the turn on
       which the unit exits the map</li>
      <li>1 VP at end of German combat phase if no US units east of
       Moselle.</li>
      <li>1 VP per US CF eliminated.</li>
      <li>Optionally, 5 VP if the US 101/502 ABIREGT unit enters the
       map.</li>
      <li>Optionally, 3 VP if the US 90/359 IREGT unit enters the
       map.</li>
     </dd>
    </dl>
   </li>
  </ul>

  <h3>Change log</h3>
  <dl>
   <dt>1.0</dt>
   <dd>Initial release of module</dd>
   <dt>1.1</dt>
   <dd>Separate layers for units and markers</dd>
  </dl>

  <h2><a name="credits"></a>Credits</h2>
  <dl>
   <dt>Design &amp; development:</dt>
   <dd>James F. Dunnigan</dd>
   <dt>Graphic design assistance &amp; inspirration:</dt>
   <dd>Redmond A. Simonsen</dd>
   <dt>Graphic production</dt>
   <dd>Ted Koller, Bob Ryer &amp; William Morrow Production
    Department</dd>
   <dt>Forbearance</dt>
   <dd>Susan Hanger</dd>
   <dt>Playtesting</dt>
   <dd>Richard Bartucchi, Gary Gillette, Dave Rodhe, Bill Watkins &amp; a
    few other folks whose names I forgot to write down</dd>
  </dl>

  <h2>Copyright and license</h2>

  <p> This work is &#127279; 2023 Christian Holm Christensen, and
   licensed under the Creative Commons Attribution-ShareAlike 4.0
   International License. To view a copy of this license, visit </p>

  <center>
   <code>http://creativecommons.org/licenses/by-sa/4.0</code>
  </center>

  <p>
    or send a letter to
  </p>

  <center>
   Creative Commons<br>
   PO Box 1866<br>
   Mountain View<br>
   CA 94042<br>
   USA
  </center>
  
</body>
</html>'''

# --------------------------------------------------------------------
hexes = {
    '01@01': '',
    '01@02': '',
    '01@03': '',
    '01@04': '',
    '01@05': 'woods',
    '01@06': '',
    '01@07': '',
    '01@08': '',
    '01@09': '',
    '01@10': '',
    '01@11': '',
    # Column 2 
    '02@01': '',
    '02@02': 'woods',
    '02@03': '',
    '02@04': '',
    '02@05': '',
    '02@06': 'town',
    '02@07': '',
    '02@08': 'town',
    '02@09': 'town',
    '02@10': '',
    '02@11': '',
    # Column 3
    '03@01': 'woods',
    '03@02': 'woods',
    '03@03': 'woods',
    '03@04': 'town',
    '03@05': 'woods',
    '03@06': 'rough',
    '03@07': 'rough',
    '03@08': 'rough',
    '03@09': 'rough',
    '03@10': 'woods',
    '03@11': 'woods',
    # Column 4
    '04@01': 'woods',
    '04@02': 'woods',
    '04@03': 'woods',
    '04@04': 'woods',
    '04@05': '',
    '04@06': '',
    '04@07': '',
    '04@08': 'woods',
    '04@09': 'woods',
    '04@10': 'woods',
    '04@11': 'woods',
    # Column 5
    '05@01': 'rough',
    '05@02': 'woods',
    '05@03': 'woods',
    '05@04': 'woods',
    '05@05': 'woods',
    '05@06': '',
    '05@07': 'woods fortified',
    '05@08': 'woods fortified',
    '05@09': 'woods fortified',
    '05@10': 'woods',
    '05@11': '',
    # Column 6
    '06@01': '',
    '06@02': 'woods',
    '06@03': 'woods',
    '06@04': 'woods',
    '06@05': 'woods',
    '06@06': 'woods fortified',
    '06@07': '',
    '06@08': 'woods',
    '06@09': 'rough fortified',
    '06@10': 'rough',
    '06@11': 'rough',
    # Column 7
    '07@01': 'town',
    '07@02': '',
    '07@03': '',
    '07@04': '',
    '07@05': '',
    '07@06': '',
    '07@07': 'fortified',
    '07@08': '',
    '07@09': 'fortified',
    '07@10': '',
    '07@11': 'rough',
    # Column 8
    '08@01': '',
    '08@02': '',
    '08@03': '',
    '08@04': '',
    '08@05': '',
    '08@06': 'fortified',
    '08@07': 'town',
    '08@08': '',
    '08@09': '',
    '08@10': 'rough',
    '08@11': 'rough',
    # Column 9
    '09@01': '',
    '09@02': '',
    '09@03': 'woods',
    '09@04': 'woods',
    '09@05': '',
    '09@06': '',
    '09@07': '',
    '09@08': '',
    '09@09': 'woods',
    '09@10': 'woods',
    '09@11': 'rough',
}
# --------------------------------------------------------------------
rivers = [
    sum([[f'07@{r:02d}.E',f'07@{r:02d}.SE'] for r in range(1,4)] +
        [['08@04.NW','08@04.NE']] +
        [[f'08@{r:02d}.E',f'08@{r:02d}.SE'] for r in range(4,7)] +
        [['08@06.SW',
         '07@07.SE',
         '07@07.SW',
         '06@07.SE',
         '06@08.E',
         '06@08.SE']] +
        [[f'06@{r:02d}.NW',f'06@{r:02d}.W'] for r in range(9,12)],[]),
    [
        '08@07.NE',
        '08@07.E',
        '08@07.SE',
        '08@07.SW',
        '08@08.W',
        '08@08.SW',
        '08@08.SE',
        '08@09.E',
        '08@09.SE',
        '08@09.SW',
        '08@10.W',
        '08@10.SW',
        '08@11.W',
      ]
]
print(rivers)
# --------------------------------------------------------------------
def concatHexes(hexes):
    return ':'+':'.join(hexes) + ':'
# --------------------------------------------------------------------
def getHexes(hexes,what):
    return [k for k,v in hexes.items() if what in v]
# --------------------------------------------------------------------
def getWoodsHexes(hexes):
    return concatHexes(getHexes(hexes,'woods'))
# --------------------------------------------------------------------
def getRoughHexes(hexes):
    return concatHexes(getHexes(hexes,'rough'))
# --------------------------------------------------------------------
def getTownHexes(hexes):
    return concatHexes(getHexes(hexes,'town'))
# --------------------------------------------------------------------
def getFortifiedHexes(hexes):
    return concatHexes(getHexes(hexes,'fortified'))
# --------------------------------------------------------------------
def addTurnPrototypes(n,
                      turns,
                      main,
                      prototypesContainer,
                      phase,
                      marker='game turn chit',
                      zoneName='Turn track'):
    # ----------------------------------------------------------------
    # Loop over turns, and create prototype for game turn marker to
    # move it along the turn track.  We do that by adding commands to
    # the turn track, which then delegates to the prototype.
    #
    # We add the prototypes to the game turn marker later on.
    board = main['mapName']
    turnp = []
    for t in range(1,n+1):
        k = key(NONE,0)+f',Turn{t}'
        r = (f'{marker}' if t == 1 else f'turn {t}@{zoneName}')
        turns.addHotkey(hotkey       = k,
                        match        = (f'{{Turn=={t}&&Phase=="{phase}"}}'),
                        reportFormat = f'=== <b>Turn {t}</b> ===',
                        name         = f'Turn {t}')
        main.addMassKey(name         = f'Turn marker to {t}',
                        buttonHotkey = k,
                        hotkey       = k,
                        buttonText   = '', 
                        target       = '',
                        filter       = (f'{{BasicName == "{marker}"}}'),
                        reportFormat = (f'{{wgDebug?"Move to turn {t}":""}}'))
        pn     = f'To turn {t}'
        traits = [
            SendtoTrait(mapName     = board,
                        boardName   = board,
                        name        = '',
                        restoreName = '',
                        restoreKey  = '',
                        zone        = zoneName,
                        destination = 'R', #R for region
                        region      = r,
                        key         = k,
                        x           = 0,
                        y           = 0),
            ReportTrait(
                k,
                report = (f'{{wgDebug?("Moving "+BasicName+" to {r}"):""}}')),
            BasicTrait()]
        prototypesContainer.addPrototype(name        = f'{pn} prototype',
                                         description = f'{pn} prototype',
                                         traits      = traits)
        turnp.append(pn)

    return turnp
# --------------------------------------------------------------------
def getRiverCrossings(rivers):
    # These are taken from the 'hexes.tex' file and then some
    # agressive Emacs qiuery-regexp-replace.
    # Meuse
    # from old import rivers
    edges = {
        ('NW','NE'): ('N',  lambda c,r:(c,r-1)),
        ('NE','E'):  ('NE', lambda c,r:(c+1,r-(c%2))),
        ('E','SE'):  ('SE', lambda c,r:(c+1,r+((c+1)%2))),
        ('SE','SW'): ('S',  lambda c,r:(c,r+1)),
        ('SW','W'):  ('SW', lambda c,r:(c-1,r+((c+1)%2))),
        ('W','NW'):  ('NW', lambda c,r:(c-1,r-(c%2)))}
    samecol = {
        'NW': 'SW',
        'NE': 'SE',
        'SW': 'NW',
        'SE': 'NE' }

    def getEdge(w1,w2):
        return edges.get((w1,w2),edges.get((w2,w1),None))
    def getTarget(edge,c1,r1):
        if edge is None:
            return ''
        targ = edge[1](c1,r1)
        if targ is None:
            return ''
        return hexName(targ[0],targ[1])
    def hexName(c,r):
        return f'{c:02d}@{r:02d}'

    from re import match

    #foo = open('crossings.tex','w')
    #print(r'\begin{scope}[line width=2pt,red,'
    #      f'dash pattern=on 0mm off 5mm on 10mm off 5mm]',file=foo)
    
    crossing = []
    for river in rivers:
        for v1,v2 in zip(river[:-1],river[1:]):
            # print(f'{v1:6s}-{v2:6s} -> ',end='')
            h1 = v1[:v1.index('.')]
            h2 = v2[:v2.index('.')]
            c1 = int(h1[:2].lstrip('0'))
            c2 = int(h2[:2].lstrip('0'))
            r1 = int(h1[3:].lstrip('0'))
            r2 = int(h2[3:].lstrip('0'))
            w1 = v1[v1.index('.')+1:]
            w2 = v2[v2.index('.')+1:]
            edge = ['?','?']
            targ = '?'
            orig = '?'
            if h1 == h2:
                edge = getEdge(w1,w2)
                if edge is None:
                    raise RuntimeError(f'Edge {w1},{w2} ({v1},{v2}) not found')

                orig = h1
                targ = getTarget(edge,c1,r1)
            else:
                if c1 == c2: # Same column
                    ww2   = samecol.get(w2,None)
                    # print(f'{v1:6s} - {v2:6s} -> {w2}')
                    if ww2 is None:
                        h1 = h2
                        c1, c2 = c2, c1
                        r1, r2 = r2, r1
                        w1, w2 = w2, w1
                        ww2 = samecol[w2]
                    if ww2 is None:
                        raise RuntimeError(f'Same vertex {w2} not found')
                    w2 = ww2
                    edge = getEdge(w1,w2)
                    orig = h1
                    targ = getTarget(edge,c1,r1)
                else:
                    # Make sure we only look right 
                    if c2 < c1:
                        c1, c2 = c2, c1
                        r1, r2 = r2, r1
                        w1, w2 = w2, w1
                        h1, h2 = h2, h1
                        #print(f'<> {h1+"."+w1}-{h2+"."+w2} ', end='')

                    assert w1 != 'W',\
                        f'Invalid first corner {w1}'
                    assert w2 != 'E',\
                        f'Invalid second corner {w2}'

                    # Only take top edge 
                    if w1 in ['SE','SW']:
                        w1 =  samecol[w1]
                        r1 += 1
                        # print(f'[1 -> {hexName(c1,r1)+"."+w1:6s}] ',end=' ')

                    if w2 in ['SE','SW']:
                        w2 =  samecol[w2]
                        r2 += 1
                        # print(f'[2 -> {hexName(c2,r2)+"."+w2:6s}] ',end='')

                    # print(f'({hexName(c1,r1)+"."+w1}-'
                    #       f'{hexName(c2,r2)+"."+w2}) -> ',end='')
                    cc = c2
                    rr = r2
                    if w1 == 'NE':
                        w1  =  'W'
                        if w2 == 'NW' and (c1 % 2 == 0) and r1 != r2:
                            rr -= 1
                            w2 =  samecol[w2]
                        if w2 == 'NW' and (c1 % 2 == 1) and r1 == r2:
                            rr -= 1
                            w2 =  samecol[w2]
                        
                    elif w1 == 'E':
                        w1   = 'NW'
                    elif w1 == 'NW':
                        assert w2 == 'W',\
                            f'w1={w1} but w2={w2}'
                        cc   = c1
                        rr   = r1
                        w2   = 'NE'

                    # print(f'{hexName(cc,rr)} {w1:2s}-{w2:2s} ->',end='')
                    edge = getEdge(w1,w2)
                    orig = hexName(cc,rr)
                    targ = getTarget(edge,cc,rr)
                        
            crossing.append([orig,targ])
            # print(f'Crossing {orig:3s} -> {targ:3s} ({edge[0]})')
            # print(fr'  \draw[->] ({orig})--({targ});'
            #       f' % {v1:6s}--{v2:6s}',file=foo)

    # print(r'\end{scope}',file=foo)
    # foo.close()
    
    return ':'+':'.join([f'{h1}{h2}:{h2}{h1}' for h1,h2 in crossing])+':'

# --------------------------------------------------------------------
# Our main patching function
def patch(build,data,vmod,gverbose=False):
    from re  import sub
    from PIL import Image
    from io  import BytesIO

    game = build.getGame()

    # --- Global properties ------------------------------------------
    debug           = 'wgDebug'
    verbose         = 'wgVerbose'
    hidden          = 'wg hidden unit'
    battleUnit      = 'wgBattleUnit'
    battleCalc      = 'wgBattleCalc'
    battleCtrl      = 'wgBattleCtlr'
    battleNo        = 'wgBattleNo'
    battleShift     = 'wgBattleShift'
    currentBattle   = 'wgCurrentBattle'
    oddsProto       = 'OddsMarkers prototype'
    globalFortified = 'FortressHexes'
    globalWoods     = 'WoodsHexes'
    globalRough     = 'RoughHexes'
    globalTown      = 'TownHexes'
    globalRivers    = 'RiverEdges'
    globalDefender  = 'DefenderHex'
    globalCombat    = 'NotCombat'
    globalRiver     = 'OverRiverEdge'
    globalDivision  = 'AttackerDivisions'# List of attacking unit division
    globalSame      = 'DivisionAttack'
    isTutorial      = 'IsTutorial'
    woodsHexes      = getWoodsHexes    (hexes)
    roughHexes      = getRoughHexes    (hexes)
    townHexes       = getTownHexes     (hexes)
    fortifiedHexes  = getFortifiedHexes(hexes)
    riverCrossing   = getRiverCrossings(rivers)
    gameTurn        = 'game turn chit'
    usVP            = 'USVP'
    deVP            = 'DEVP'
    hasVP           = 'HasVPs'
    disableVP       = 'DisableVP'
    
    # --- Internal keys ----------------------------------------------
    updateDefender = key(NONE,0)+',updateDefender'
    updateRiver    = key(NONE,0)+',updateRiver'
    resetRiver     = key(NONE,0)+',resetRiver'
    updateDivision = key(NONE,0)+',updateDivision'
    resetDivision  = key(NONE,0)+',resetDivision'
    checkCombat    = key(NONE,0)+',checkCombat'
    advanceTurn    = key(NONE,0)+',advanceTurn'
    nextFaction    = key(NONE,0)+',nextFaction'
    checkSetup     = key(NONE,0)+',checkSetup'
    nextPhase      = key('T',ALT) # key(NONE,0)+',nextPhase'    
    resolveKey     = key('Y')
    setBattle      = key(NONE,0)+',wgSetBattle'
    dieRoll        = key(NONE,0)+',dieRoll'
    optKey         = key('Q',ALT)
    deleteKey      = key(NONE,0)+',delete'
    setitupKey     = key(NONE,0)+',setitup'
    toggleKey      = key(NONE,0)+',toggleOpt'
    setOptional    = key(NONE,0)+',setOpt'
    cleanOptional  = key(NONE,0)+',cleanOpt'
    printKey       = key(NONE,0)+',print'
    tutorialKey    = key(NONE,0)+',isTutorial'
    fixVPs         = key(NONE,0)+',fixVPs'
    calcVPs        = key(NONE,0)+',calcVPs'
    resetVPs       = key(NONE,0)+',resetVPs'
    calcUSVPs      = calcVPs+'US'
    calcDEVPs      = calcVPs+'DE'
    showVPs        = key('V',ALT)
    showWinner     = key(NONE,0)+',showWinner'
    
    # ----------------------------------------------------------------
    # Extra documentation 
    doc  = game.getDocumentation()[0]
    more = doc.addHelpFile(title='More information',fileName='help/more.html')
    vmod.addFile('help/more.html', moreHelp.format(title=game['name']))

    # --- Global properties ------------------------------------------
    gp              = game.getGlobalProperties()[0];
    for h,n in [[woodsHexes,        globalWoods     ],   
                [roughHexes,        globalRough     ],   
                [townHexes,         globalTown      ],   
                [fortifiedHexes,    globalFortified ],   
                [riverCrossing,     globalRivers    ],
                ['',                globalDefender  ],
                ['',                globalDivision  ],
                [-1,                globalRiver     ],
                [0,                 globalSame      ],
                ['false',           isTutorial      ],
                [0,                 usVP            ],
                [0,                 deVP            ],
                ['true',            disableVP       ],
                ]:
        gp.addProperty(name         = n,
                       initialValue = h,
                       description  = f'List of {n} hexes')
    
    # --- Names of phases --------------------------------------------
    phaseNames     = ['Setup',
                      'US movement',
                      'US combat',
                      'DE movement',
                      'DE combat']
    setupPhase = phaseNames[0]
    usMove     = phaseNames[1]
    usCombat   = phaseNames[2]
    deMove     = phaseNames[3]
    deCombat   = phaseNames[4]

    # ----------------------------------------------------------------
    # Get the turns interface, and set up.  We built a set of traits
    # to add to the game turn marker to progress the turn track.
    turns                 = game.getTurnTracks()['Turn']
    turns['reportFormat'] = '{"<b>Phase</b>:  <u>"+Phase+"</u>"}'
    phases                = turns.getLists()['Phase']
    phases['list']        = ','.join(phaseNames)
    
    
    # ================================================================
    #
    # Options
    #
    # ----------------------------------------------------------------
    optDivision   = 'optDivision'
    optWithdrawal = 'optWithdrawal'
    optAirborne   = 'optAirborne'
    for o, t in [[optDivision  , 'Division'],
                 [optWithdrawal, 'Withdrawal'],
                 [optAirborne  , 'Airborne']]:
        gp.addProperty(name         = o,
                       initialValue = 'true',
                       description  = t)


    # ----------------------------------------------------------------
    # Dice
    diceName  = '1d6Dice'
    diceKey   = key('6',ALT)
    dices     = game.getSymbolicDices()
    for dn, dice in dices.items():
        if dn == '1d6Dice':
            dice['hotkey'] = diceKey

    game.getPieceWindows()['Counters']['icon'] = 'unit-icon.png'
        
    # ----------------------------------------------------------------
    prototypeContainer = game.getPrototypes()[0]
    # ----------------------------------------------------------------
    maps = game.getMaps()
    main = maps['Board']

    # ----------------------------------------------------------------
    # Delete key
    dkey = main.getMassKeys().get('Delete',None)
    if dkey is not None:
        dkey['icon'] = ''

    # ----------------------------------------------------------------
    # Add layers to the map
    pieceLayer      = 'PieceLayer'
    unitLayer       = 'Units'
    btlLayer        = 'Units' #'Battle' - doesn't work with auto odds
                              #if not same as units - anyways, the
                              #markers should stack with units.
    oddLayer        = 'Odds'
    resLayer        = 'Result'
    objLayer        = 'Objectives'
    layerNames = {objLayer:  {'t': objLayer,  'i': ''},
                  unitLayer: {'t': unitLayer, 'i': '' },
                  #btlLayer,
                  oddLayer: {'t': oddLayer+' markers', 'i': ''},
                  resLayer: {'t': resLayer+' markers', 'i': ''} }
                  
    layers = main.addLayers(description='Layers',
                            layerOrder=layerNames)
    for lt,ln in layerNames.items():
        layers.addControl(name       = lt,
                          tooltip    = f'Toggle display of {ln}',
                          text       = f'Toggle {ln["t"]}',
                          icon       = ln['i'],
                          layers     = [lt])
    layers.addControl(name    = 'Show all',
                      tooltip = f'Show all',
                      text    = f'Show all',
                      command = LayerControl.ENABLE,
                      layers  = list(layerNames.keys()))

    lmenu = main.addMenu(description = 'Toggle layers',
                         text        = '',
                         tooltip     = 'Toggle display of layers',
                         icon        = 'layer-icon.png',
                         menuItems   = ([f'Toggle {ln["t"]}'
                                         for ln in layerNames.values()]
                                        +['Show all']))
    hideP  = main.getHidePiecesButton()
    if len(hideP) > 0:
        print('Found hide pieces button, moving layers before that')
        main.remove(layers)
        main.remove(lmenu)
        main.insertBefore(layers,hideP[0])
        main.insertBefore(lmenu, hideP[0])
    else:
        print('Hide pueces button not found',hideP)
    
    # ----------------------------------------------------------------
    # Get optionals map
    opts = maps['Optionals']
    opts['allowMultiple'] = False
    opts['markMoved']     = 'Never'
    opts['hotkey']        = optKey
    opts['launch']        = True
    opts['icon']          = 'opt-icon.png'
    opts.remove(opts.getImageSaver()[0])
    opts.remove(opts.getTextSaver()[0])
    opts.remove(opts.getGlobalMap()[0])
    opts.remove(opts.getHidePiecesButton()[0])
    obrd  = opts.getBoardPicker()[0].getBoards()['Optionals']
    okeys = opts.getMassKeys()
    for kn,k in okeys.items():
        opts.remove(k) 
    ostart = opts.getAtStarts(False)
    for at in ostart:
        if at['name'] == hidden:
            opts.remove(at)

    game.addStartupMassKey(name        = 'Optionals',
                           hotkey      = optKey,
                           target      = '',
                           filter      = f'{{BasicName=="{hidden}"}}',
                           whenToApply = StartupMassKey.START_GAME,
                           reportFormat=
                           f'{{{debug}?("~ Show optionals window"):""}}')


    
    # ----------------------------------------------------------------
    # Get Zoned area
    zoned = main.getBoardPicker()[0].getBoards()['Board'].getZonedGrids()[0]
    high  = zoned.getHighlighters()[0]
    
    # ----------------------------------------------------------------
    # Adjust the hexes
    #
    # Get the hex grid and turn of drawing
    zones  = zoned.getZones()
    hzone  = zones['hexes']
    hgrids = hzone.getHexGrids()
    hgrid  = hgrids[0]
    hnum   = hgrid.getNumbering()[0]
    # print(f'grid offsets: h={hnum["hOff"]} v={hnum["vOff"]}')
    # hnum['hOff']  =  0
    hnum['vOff']  = int(hnum['vOff']) - 1
    hnum['sep']   = '@'

    for zn,zc in zip(['west','east','south'],
                     [(255,0,0),(0,255,0),(0,0,255)]):
        pn = f'{zn}Highlight'
        hn = f'{zn}High'
        zone = zones[zn]
        grid = zone.getSquareGrids()[0]
        zone.remove(grid)
        zone.addProperty(name         = pn,
                         initialValue = hn,
                         description  = f'{zn} highligher')
        high.addZoneHighlight(name    = hn,
                              color   = rgb(*zc),
                              opacity = 75)
        zone['highlightProperty'] = pn
        zone['useHighlight']      = True
        zone['locationFormat']    = f'{zn.capitalize()} edge'


    # ----------------------------------------------------------------
    # Specific actions 
    turns.addHotkey(hotkey       = cleanOptional,
                    match        = f'{{Phase=="{usMove}"&&Turn==1}}',
                    reportFormat = (f'{{{debug}?"~Cleaning for optional rules":'
                                    f'""}}'),
                    name         = 'CleanOptional')
    turns.addHotkey(hotkey = checkSetup,
                    match  = '{Phase=="Setup"&&Turn==1}',
                    name   = 'Check for setup phase')
    turns.addHotkey(hotkey = calcVPs+'Turn',
                    match  = f'{{Phase=="Setup"}}',
                    name   = 'Calculate VPs on start of turn')
    turns.addHotkey(hotkey = showWinner,
                    match  = '{Turn > 7}',
                    name   = 'Show the winner'),
    turns.addHotkey(hotkey = nextPhase,
                    match  = '{Phase=="Setup"&&Turn>1}',
                    name   = 'Skip setup phase on later turns')
    
    # ----------------------------------------------------------------
    # Turn track progress 
    turnp = addTurnPrototypes(7,turns,main,prototypeContainer,
                              usMove, #phaseNames[0],
                              marker=gameTurn,
                              zoneName='Turns')

    # ----------------------------------------------------------------
    # Add some global keys to the map
    # ----------------------------------------------------------------
    curBtl        = (f'{{{battleNo}=={currentBattle}&&'
                    f'{battleUnit}==true}}')
    curDef        = (f'{{{battleNo}=={currentBattle}&&'
                     f'{battleUnit}==true&&'
                     f'IsAttacker==false}}')
    curAtt        = (f'{{{battleNo}=={currentBattle}&&'
                     f'{battleUnit}==true&&'
                     f'IsAttacker==true}}')
     
    # ----------------------------------------------------------------
    # - Store defender hex
    main.addMassKey(
        name         = 'Store defender hex',
        buttonHotkey = updateDefender,
        buttonText   = '',
        singleMap    = True,
        hotkey       = updateDefender,
        target       = '',
        filter       = curDef,
        reportFormat = (f'{{{debug}?("Mass update defender hex"):""}}'))
    main.addMassKey(
        name         = 'Store attacker division',
        buttonHotkey = updateDivision,
        buttonText   = '',
        singleMap    = True,
        hotkey       = updateDivision,
        target       = '',
        filter       = curAtt,
        reportFormat = (f'{{{debug}?("Mass update attacker divisions"):""}}'))
    main.addMassKey(
        name         = 'Store attacker river',
        buttonHotkey = updateRiver,
        buttonText   = '',
        singleMap    = True,
        hotkey       = updateRiver,
        target       = '',
        filter       = curAtt,
        reportFormat = (f'{{{debug}?("Mass update attacker river edge"):""}}'))
    main.addMassKey(name         = 'Clean optional US airborne',
                    buttonHotkey = cleanOptional,
                    buttonText   = '',
                    hotkey       = deleteKey,
                    singleMap    = False,
                    target       = '',
                    filter       = (f'{{{optAirborne}==false&&'
                                    f'BasicName=="us 101 502 air"}}'),
                    reportFormat = (f'{{{debug}?("~ Remove us 101 502 air "+'
                                    f'({optAirborne}?"no":"yes")):""}}'))
    main.addMassKey(name         = 'Fix VPs',
                    buttonHotkey = fixVPs,
                    hotkey       = fixVPs,
                    singleMap    = True,
                    target       = '',
                    filter       = f'{{{hasVP}==true}}',
                    reportFormat = (f'{{{debug}?("~ Fix VPs"):""}}'))
    main.addMassKey(name         = 'Calculate VPs',
                    buttonHotkey = calcVPs,
                    hotkey       = calcVPs,
                    singleMap    = True,
                    target       = '',
                    filter       = f'{{{hasVP}==true}}',
                    reportFormat = (f'{{{debug}?("~ Mass update VPs"):""}}'))
    main.addMassKey(name         = 'Calculate VPs',
                    buttonHotkey = calcVPs+'Turn',
                    hotkey       = calcVPs,
                    singleMap    = True,
                    target       = '',
                    filter       = f'{{BasicName=="{hidden}"}}',
                    reportFormat = (f'{{{debug}?("~ Update VPs"):""}}'))
    main.addMassKey(name         = 'Show VPs',
                    buttonText   = 'VP',
                    tooltip      = 'Show VPs',
                    buttonHotkey = showVPs,
                    hotkey       = '',
                    singleMap    = True,
                    target       = '',
                    filter       = '',
                    reportFormat = (f'{{"<b>VPs</b> <u>US="+{usVP}+" DE="+{deVP}+"</u>"}}'))
    main.addMassKey(name         = 'Show winner',
                    buttonText   = '',
                    buttonHotkey = showWinner,
                    hotkey       = '',
                    singleMap    = True,
                    target       = '',
                    filter       = '',
                    reportFormat = (
                        f'{{Alert("US VPs: "+{usVP}+", German VPs: "+{deVP}+", "'
                        f'+({usVP}>{deVP}?"US":({deVP}>{usVP}?"German":"No"))'
                        f'+" faction wins")}}'))


    # ================================================================
    #
    # Prototypes
    #
    # ----------------------------------------------------------------
    # Clean up prototypes
    prototypes         = prototypeContainer.getPrototypes(asdict=False)
    seen       = list()
    for p in prototypes:
        if p['name'] == ' prototype':
            prototypes.remove(p)
        if p['name'] in seen:
            # print(f'Removing prototype {p["name"]}')
            prototypes.remove(p)
        seen.append(p['name'])

    prototypes     = prototypeContainer.getPrototypes(asdict=True)
    
    # ----------------------------------------------------------------
    # Modify Markers prototype
    markersP     = prototypes['Markers prototype']
    traits       = markersP.getTraits()
    mdel         = Trait.findTrait(traits,DeleteTrait.ID)
    if mdel:
        mdel['key']  = deleteKey
        mdel['name'] = ''
    else:
        traits.insert(0,DeleteTrait(name='',key=deleteKey))
    markersP.setTraits(*traits)

    # ----------------------------------------------------------------
    # Modify controls prototype
    controlP  = prototypes['ControlMarkers prototype']
    traits    = controlP.getTraits()
    basic     = traits.pop()
    controlP.setTraits(
        MarkTrait(
            name       = hasVP,
            value      = True),
        MarkTrait(
            name       = pieceLayer,
            value      = objLayer),
        CalculatedTrait(
            name       = 'Faction',
            expression = '{(Step_Level==2)?"US":"DE"}'),
        NoStackTrait(move=NoStackTrait.NEVER_MOVE,
                     bandSelect=NoStackTrait.NEVER_BAND_SELECT),
        GlobalPropertyTrait(
            ['',calcVPs,GlobalPropertyTrait.DIRECT,
             f'{{{usVP}+(Faction=="US"?VP:0)}}'],
            name    = usVP,
            numeric = True),
        ReportTrait(calcVPs,
                    report=(f'{{{debug}?("!"+LocationName+" ("+VP+")"+'
                            f'" under "+Faction+" control -> "+'
                            f'{usVP}):""}}')),
        basic)
    
    # ----------------------------------------------------------------
    # Modify odds prototype
    oddsP          = prototypes['OddsMarkers prototype']
    traits = oddsP.getTraits()
    basic  = traits.pop()
    res    = Trait.findTrait(traits,CalculatedTrait.ID,
                             key='name',value='BattleResult')
    trg    = Trait.findTrait(traits,TriggerTrait.ID,
                             key='key',value=resolveKey)
    crt    = (f'wgBattleIdx==1?(Die<3?"--":"A1"):'#<= 1
              f'wgBattleIdx==2?(Die==1?"D1":(Die<4?"--":"A1")):'# 0
              f'wgBattleIdx==3?(Die<3?"D1":(Die==3?"--":"A1")):'# 1
              f'wgBattleIdx==4?(Die<4?"D1":(Die==4?"--":"A1")):'# 2-3
              f'wgBattleIdx==5?(Die<2?"D2":(Die<5?"D1":(Die==5?"--":"A1"))):'# 4-5
              f'wgBattleIdx==6?(Die<3?"D2":(Die<6?"D1":"--")):'# 6-7
              f'wgBattleIdx==7?(Die<4?"D2":"D1"):'# 8-9
              f'wgBattleIdx==8?(Die<5?"D2":"D1"):'# >= 10
              f'"--"')
    res['expression'] = f'{{{crt}}}'
    traits.append(MarkTrait(name = pieceLayer, value = oddLayer))
    oddsP.setTraits(*traits,basic)

    # ----------------------------------------------------------------
    # Modify marker prototype
    btlP   = prototypes['BattleMarkers prototype']
    traits = btlP.getTraits()
    basic  = traits.pop()
    traits.append(MarkTrait(name = pieceLayer, value = btlLayer))
    btlP.setTraits(*traits,basic)
    
    # ----------------------------------------------------------------
    # Modify results prototype
    resP   = prototypes['ResultMarkers prototype']
    traits = resP.getTraits()
    basic  = traits.pop()
    traits.append(MarkTrait(name = pieceLayer, value = resLayer))
    resP.setTraits(*traits,basic)
    
    # ----------------------------------------------------------------
    # Modify optionals prototype
    optionalP = prototypes['OptionalMarkers prototype']
    traits    = optionalP.getTraits()
    basic     = traits.pop()
    optionalP.setTraits(
        TriggerTrait(name       = '',
                     key        = toggleKey,
                     actionKeys = [setOptional],
                     property   = f'{{Phase=="Setup"}}'),
        ClickTrait(key = toggleKey,
                   context = False,
                   whole = True,
                   description = 'Toggle optional rule'),
        ReportTrait(setOptional,
                    report=(f'{{{debug}?("! "+BasicName+" toggle "+'
                            f'Name+" "+'
                            f'(Step_Level==1?"On":"Off")):""}}')),
        basic)
    

    # ----------------------------------------------------------------
    # Battle unit prototype update with forest, fortress, overriver rules
    #
    # Need a bit more massage 
    battleUnitP = prototypes[battleUnit]
    traits      = battleUnitP.getTraits()
    basic       = traits.pop()
    oddsS       = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name',value='OddsShift')
    oddsS['expression'] = (f'{{(!IsAttacker)?(InFortified?-3:'
                           f'(InWoods||InTown)?-2:InRough?-1:0):'
                           f'wgBattleShift}}')
    traits.extend([
        CalculatedTrait(
            name        = 'InWoods',
            expression  = (f'{{{globalWoods}.contains(":"+'
                           f'LocationName+":")}}'),
            description = 'Defender in woods hex?'),
        CalculatedTrait(
            name        = 'InRough',
            expression  = (f'{{{globalRough}.contains(":"+'
                           f'LocationName+":")}}'),
            description = 'Defender in rough hex?'),
        CalculatedTrait(
            name        = 'InTown',
            expression  = (f'{{{globalTown}.contains(":"+'
                           f'LocationName+":")}}'),
            description = 'Defender in town hex?'),
        CalculatedTrait(
            name        = 'InFortified',
            expression  = (f'{{{globalFortified}.contains(":"+'
                           f'LocationName+":")}}'),
            description = 'Defender in fortified hex?'),
        CalculatedTrait(
            name        = 'OverRiver',
            expression  = (f'{{{globalRivers}.contains(":"+'
                           f'{globalDefender}+LocationName+":")}}'),
            description = 'Check if unit attacks across river'),
        CalculatedTrait(
            name        = 'IndexFirst',
            expression  = f'{{{globalDivision}.indexOf(":"+Division)}}',
            description = 'First index of own division'),
        CalculatedTrait(
            name        = 'IndexLast',
            expression  = f'{{{globalDivision}.lastIndexOf(":"+Division)}}',
            description = 'Last index of own division'),
        CalculatedTrait(
            name        = 'SameDivision',
            expression  = f'{{IndexFirst!=IndexLast}}',
            description = 'Other units of same division'),
        TriggerTrait(
            name       = '',
            key        = updateDefender,
            actionKeys = [f'{updateDefender}Real'],
            property   = '{!IsAttacker}'),
        TriggerTrait(
            name       = '',
            key        = updateDivision,
            actionKeys = [f'{updateDivision}Real',
                          f'{updateDivision}Same'],
            property   = f'{{IsAttacker&&{optDivision}}}'),
        GlobalPropertyTrait(
            ['',f'{updateDefender}Real',GlobalPropertyTrait.DIRECT,
             f'{{LocationName}}'],
            name        = f'{globalDefender}',
            numeric     = False,
            description = 'Update defender hex'),
        GlobalPropertyTrait(
            ['',f'{updateDivision}Real',GlobalPropertyTrait.DIRECT,
             f'{{{globalDivision}+":"+Division}}'],
            name        = f'{globalDivision}',
            numeric     = False,
            description = 'Update attacker divisions'),
        GlobalPropertyTrait(
            ['',updateDivision+'Same',GlobalPropertyTrait.DIRECT,
             f'{{(IsAttacker&&SameDivision)?1:{globalSame}}}'
             ],
            name        = globalSame,
            numeric     = True,
            description = 'Update whether division attack',
        ),
        GlobalPropertyTrait(
            ['',updateRiver,GlobalPropertyTrait.DIRECT,
             f'{{(IsAttacker?Math.max((OverRiver?-3:0),{globalRiver}):{globalRiver})}}'
             ],
            name        = globalRiver,
            numeric     = True,
            description = 'Update whether over river edge',
        ),
        ReportTrait(
            updateDefender,
            report = (f'{{{debug}?("~"+BasicName+" "+LocationName'
                      f'+" Defender hex: "+{globalDefender}):""}}')),
        ReportTrait(
            updateDivision,
            report = (f'{{{debug}?("~"+BasicName+" "+Division'
                      f'+" Attacker divisions: "+{globalDivision}):""}}')),
        ReportTrait(
            updateRiver,
            report = (f'{{{debug}?("~"+BasicName+" "+LocationName'
                      f'+" update river: "+{globalRiver}):""}}')),
        ReportTrait(
            updateDivision+'Same',
            report = (f'{{{debug}?("~"+BasicName+" "+LocationName'
                      f'+" update division (same): "+{globalSame}'
                      f'+" divisions="+{globalDivision}'
                      f'+" same as this="+SameDivision):""}}')),
        TriggerTrait(
            name       = '',
            command    = 'Print',
            key        = printKey,
            property   = f'{{{debug}}}',
            actionKeys = []),
        ReportTrait(
            printKey,
            report = (f'{{"~"+BasicName+" "+LocationName'
                      f'+" Division: "+Division'
                      f'+" In woods: "+InWoods'
                      f'+" In rough: "+InRough'
                      f'+" In Town: "+InTown'
                      f'+" In Fortified: "+InFortified'
                      f'+" Over river: "+OverRiver'
                      f'+" Divisions: "+{globalDivision}'
                      f'+" 1st index: "+IndexFirst'
                      f'+" 2nd index: "+IndexLast'
                      f'+" Same: "+SameDivision'
                      f'+""}}')),
    ])
    battleUnitP.setTraits(*traits,basic)
    
    # ----------------------------------------------------------------
    # Battle calculation update with forest, fortress, overriver rules
    battleCalcP = prototypes[battleCalc]
    traits      = battleCalcP.getTraits()
    basic       = traits.pop()
    calcShift   = Trait.findTrait(traits,TriggerTrait.ID,
                                  key='key',         
                                  value=key(NONE,0)+',wgCalcBattleShift')
    shiftProp   = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name',value='OddsShift')
    shiftProp['expression'] = f'{{Math.min(wgBattleShift,{globalRiver})+{globalSame}}}'
    oddsRProp   = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name',value='OddsIndexRaw')
    oddsRProp['expression'] = (f'{{'
                               f'(BattleFraction<=-1)?1:'
                               f'(BattleFraction==0)?2:'
                               f'(BattleFraction==1)?3:'
                               f'(BattleFraction==2||BattleFraction==3)?4:'
                               f'(BattleFraction==4||BattleFraction==5)?5:'
                               f'(BattleFraction==6||BattleFraction==7)?6:'
                               f'(BattleFraction==8||BattleFraction==9)?7:'
                               f'(BattleFraction>=10)?8:0}}')
    oddsIProp   = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name',value='OddsIndex')
    oddsIProp['expression'] = f'{{Math.max(1,Math.min(OddsIndexRaw+OddsShift,8))}}'

    traits.extend([
        GlobalPropertyTrait(
            ['',resetRiver,GlobalPropertyTrait.DIRECT,'{-3}'],#Assume river
            name        = globalRiver,
            numeric     = True,
            description = 'Reset river odds shift',
        ),
        GlobalPropertyTrait(
            ['',resetDivision,GlobalPropertyTrait.DIRECT,'{0}'],
            name        = globalSame,
            numeric     = True,
            description = 'Reset division attack odds shift',
        ),
        GlobalPropertyTrait(
            ['',resetDivision,GlobalPropertyTrait.DIRECT,''],
            name        = globalDivision,
            numeric     = True,
            description = 'Reset division attack odds shift',
        ),
        GlobalHotkeyTrait(
            name         = '',
            key          = updateDefender,
            globalHotkey = updateDefender,
            description  = 'Ask GKC to update defender hex'),
        GlobalHotkeyTrait(
            name         = '',
            key          = updateDivision,
            globalHotkey = updateDivision,
            description  = 'Ask GKC to update division attack'),
        GlobalHotkeyTrait(
            name         = '',
            key          = updateRiver,
            globalHotkey = updateRiver,
            description  = 'Ask GKC to update shift'),
        ReportTrait(
            resetDivision,
            report = (f'{{{debug}?("! "+BasicName+" Reset division: "+'
                      f'{globalDivision}+" - "+{globalSame}):""}}')),
        ReportTrait(
            resetRiver,
            report = (f'{{{debug}?("! "+BasicName+" Reset river: "+'
                      f'{globalRiver}):""}}')),
        ReportTrait(
            updateDefender,
            report = (f'{{{debug}?("! "+BasicName+" Update hex "+'
                      f'{globalDefender}):""}}')),
        ReportTrait(
            updateDivision,
            report = (f'{{{debug}?("! "+BasicName+" Update division "+'
                      f'{globalSame}):""}}')),
        ReportTrait(
            updateRiver,
            report = (f'{{{debug}?("! "+BasicName+" Update river "+'
                      f'{globalRiver}):""}}')),
        ReportTrait(
            key(NONE,0)+',wgCalcBattleShiftTrampoline',
            report = (f'{{{debug}?("! "+BasicName+" Calc battle shift: "+'
                      f'"Raw="+OddsIndexRaw+'
                      f'" Shift="+OddsShift+'
                      f'" (Divsion="+{globalSame}+" River="+{globalRiver}+")"+'
                      f'" -> "+wgBattleShift):""}}')),
        ReportTrait(
            key(NONE,0)+',wgCalcBattleFracReal',
            report = (f'{{{debug}?("! "+BasicName+" Calc battle frac: "+'
                      f'"BattleFraction="+BattleFraction+'
                      f'" AF="+wgBattleAF+'
                      f'" DF="+wgBattleDF):""}}')),
        basic])
    keys = calcShift.getActionKeys()
    # print(keys,calcShift['actionKeys'])
    calcShift.setActionKeys([resetRiver,
                             resetDivision,
                             updateDefender,
                             updateRiver,
                             updateDivision]+keys)
    battleCalcP.setTraits(*traits)
        
    
    # ----------------------------------------------------------------
    # Modify faction prototypes 
    for faction in ['DE', 'US']:
        factionp = prototypes[f'{faction} prototype']
        ftraits  = factionp.getTraits()
        fbasic   = ftraits.pop()
        
        # Remove traits we don't want nor need
        fdel     = Trait.findTrait(ftraits,DeleteTrait.ID)
        if fdel:
            fdel['key'] = deleteKey
            fdel['name'] = ''
        else:
            ftraits.insert(0,DeleteTrait(name='',key=deleteKey))

        ftraits.extend([
            MarkTrait(
                name        = hasVP,
                value       = True),
            MarkTrait(
                name       = pieceLayer,
                value      = unitLayer),
            CalculatedTrait(
                name        = 'ExitBoard',
                expression  = f'{{LocationName=="offboard"}}',
                description = 'Unit exited board'),
            CalculatedTrait(
                name        = 'ExitWest',
                expression  = f'{{CurrentZone=="west"}}',
                description = 'Unit exited board on West edge'),
            CalculatedTrait(
                name        = 'ExitEast',
                expression  = f'{{CurrentZone=="east"}}',
                description = 'Unit exited board on West edge'),
            CalculatedTrait(
                name        = 'ExitSouth',
                expression  = f'{{CurrentZone=="south"}}',
                description = 'Unit exited board on south edge'),
        ])
        
        if faction == 'US':
            eastMoselle = ['0801', '0901',
                           '0802', '0902',
                           '0803', '0903',
                           '0904',
                           '0905',
                           '0906',
                           '0807', '0907',
                           '0708', '0808','0908',
                           '0609', '0709','0809','0909',
                           '0610', '0710','0810','0910',
                           '0611', '0711','0811','0911']
            eastOf = '||'.join([f'LocationName=="{h[0:2]}@{h[2:]}"'
                                for h in eastMoselle])
            ftraits.extend([
                CalculatedTrait(
                    name        = 'EastOfMoselle',
                    expression  = f'{{{eastOf}}}',
                    description = 'US unit east of Moselle'),
                # Check if east of mosellse, then true,
                # Otherwise, if was east of moselle for three turns,
                # but now off-board, keep count, otherwise, 0
                #
                # Perhaps fix if _ever_ 3 turns east of moselle?
                DynamicPropertyTrait(
                    ['',fixVPs,DynamicPropertyTrait.DIRECT,
                     ('{EastOfMoselle?(MoselleTurns+1):'                      
                      '((MoselleTurns>=3&&ExitBoard)?MoselleTurns:0)}')],
                    name = 'MoselleTurns',
                    numeric = True,
                    value = 0),
                CalculatedTrait(
                    name        = 'MoselleVP',
                    expression  = '{(MoselleTurns>=3)?5:0}',
                    description = 'VPs for being east of Moselle'),
                CalculatedTrait(
                    name        = 'ExitVP',
                    expression  = '{ExitBoard||ExitEast?5:0}',
                    description = 'VPs for exiting board'),
                GlobalPropertyTrait(
                    ['',calcVPs,GlobalPropertyTrait.DIRECT,
                     f'{{{usVP}+ExitVP+MoselleVP}}'],
                    name        = usVP,
                    numeric     = True,
                    description = 'Increment US VPs'),
                ReportTrait(
                    fixVPs,
                    report = (f'{{{debug}?("!"+BasicName'
                              f'+" Turns east of Moselle:"+MoselleTurns):'
                              f'""}}')),
                ReportTrait(
                    calcVPs,
                    report = (f'{{{debug}?("!"+BasicName'
                              f'+" EastOfMoselle:"+EastOfMoselle'
                              f'+" For turns:"+MoselleTurns'
                              f'+" VPs:"+MoselleVP'
                              f'+" ExitEast:"+ExitEast'
                              f'+" ExitBoard:"+ExitBoard'
                              f'+" VPs:"+ExitVP'
                              f'+" Total:"+(ExitVP+MoselleVP)):""}}'))
            ])
        else: # German
            ftraits.extend([
                DynamicPropertyTrait(
                    ['',fixVPs,DynamicPropertyTrait.DIRECT,
                     ('{Retreated>0?Retreated:'
                      '((Type.contains("armoured")&&'
                      '(ExitBoard||ExitSouth||ExitEast))?Turn-1:0)}')],
                    name    = 'Retreated',
                    numeric = True,
                    value   = 0),
                CalculatedTrait(
                    name        = 'RetreatVP',
                    expression  = '{(Retreated>0)?(8-Retreated):0}',
                    description = 'VPs for volunetary armoured retreat'),
                CalculatedTrait(
                    name        = 'ExitVP',
                    expression  = '{Retreated>0?0:(ExitWest?10:0)}',
                    description = 'VPs for leaving over west edge'),
                GlobalPropertyTrait(
                    ['',calcVPs,GlobalPropertyTrait.DIRECT,
                     f'{{{deVP}+ExitVP+RetreatVP}}'],
                    name        = deVP,
                    numeric     = True,
                    description = 'Increment US VPs'),
                ReportTrait(
                    fixVPs,
                    report = (f'{{{debug}?("!"+BasicName'
                              f'+" Retreated turn:"+Retreated):'
                              f'""}}')),
                ReportTrait(
                    calcVPs,
                    report = (f'{{{debug}?("!"+BasicName'
                              f'+" Type:"+Type'
                              f'+" Retreated:"+Retreated'
                              f'+" VPs:"+RetreatVP'
                              f'+" ExitEast:"+ExitEast'
                              f'+" ExitSouth:"+ExitSouth'
                              f'+" ExitWest:"+ExitWest'
                              f'+" ExitBoard:"+ExitBoard'
                              f'+" VPs:"+ExitVP'
                              f'+" Total:"+(RetreatVP+ExitVP)):""}}'))])

        # ftraits.remove(fdel)
        factionp.setTraits(*ftraits,fbasic)

    # ================================================================
    #
    # Get all pieces and modify them
    #
    pieces  = game.getPieces(asdict=False)
    for piece in pieces:
        name    = piece['entryName'].strip()
        faction = name[:3]
        Faction = 'DE' if faction in ['de ','ss '] else 'US'
        
        if name.startswith('opt '):
            # Options marker 
            optName = name.replace('opt ','').strip()
            gpName  = name.replace(' ','')
            
            traits = piece.getTraits()
            basic  = traits.pop()
            step   = Trait.findTrait(traits,LayerTrait.ID,
                                     key = 'name',
                                     value = 'Step')
            step['newNames']     = ['+ enabled','+ disabled']
            step['increaseName'] = ''
            step['increaseKey']  = ''
            step['follow']       = True
            step['expression']   = f'{{{gpName}==true?1:2}}'
            traits.extend([
                MarkTrait(name='OptName',value=optName),
                GlobalPropertyTrait(
                    ["",setOptional,GlobalPropertyTrait.DIRECT,
                     f'{{!{gpName}}}'],
                    name = gpName,
                    numeric = True,
                    description = f'Toggle {gpName}'),
                basic
            ])
            piece.setTraits(*traits)

        elif name == hidden:
            traits = piece.getTraits()
            basic  = traits.pop()
            showN  = key(NONE,0)+',showNotes'
            showV  = key(NONE,0)+',showVPs'
            traits = [
                TriggerTrait(
                    name       = 'Show notes window for tutorial',
                    command    = '',
                    key        = tutorialKey,
                    property   = f'{{{isTutorial}==true}}',
                    actionKeys = [showN]),
                GlobalHotkeyTrait(
                    name         = '',
                    key          = showN,
                    globalHotkey = key('N',ALT),
                    description  = 'Show notes window'),
                GlobalHotkeyTrait(
                    name         = '',
                    key          = optKey,
                    globalHotkey = optKey,
                    description  = 'Show optional rules'),
                GlobalPropertyTrait(
                    ['',resetVPs,GlobalPropertyTrait.DIRECT,'{0}'],
                    name        = usVP,
                    numeric     = True,
                    description = 'Zero US VPs'), 
               GlobalPropertyTrait(
                    ['',resetVPs,GlobalPropertyTrait.DIRECT,'{0}'],
                    name        = deVP,
                    numeric     = True,
                    description = 'Zero DE VPs'),
                GlobalHotkeyTrait(
                    name         = '',
                    key          = calcVPs+'Trampoline',
                    globalHotkey = calcVPs,
                    description  = 'Calculate VPs'),
                GlobalHotkeyTrait(
                    name         = '',
                    key          = fixVPs+'Trampoline',
                    globalHotkey = fixVPs,
                    description  = 'Fix VPs'),
                GlobalHotkeyTrait(
                    name         = '',
                    key          = showV,
                    globalHotkey = showVPs,
                    description  = 'Show VPs'),
                TriggerTrait(
                    name       = 'Calculate VPs',
                    command    = '',
                    key        = calcVPs,
                    actionKeys = [resetVPs,
                                  fixVPs+'Trampoline',
                                  calcVPs+'Trampoline',
                                  showV]),
            ]+traits
            piece.setTraits(*traits,basic)

        elif name == gameTurn:
            # Game turn marker 
            gtraits  = piece.getTraits()
            gbasic   = gtraits.pop()
            markers  = Trait.findTrait(gtraits,
                                       PrototypeTrait.ID,
                                       key = 'name',
                                       value = 'Markers prototype')
            if markers is not None:
                gtraits.remove(markers)
            
            # Add the prototypes we created above 
            gtraits.extend([PrototypeTrait(pnn + ' prototype')
                            for pnn in turnp])
            gtraits.append(RestrictAccessTrait(sides=[],
                                               description='Cannot move'))
            gtraits.append(gbasic)
            piece.setTraits(*gtraits)

        elif name == 'control':
            cps = {'07@01': 5,
                   '08@07': 20}

            for vhex, vp in cps.items():
                cpy = main.addAtStart(name            = f'VP {vhex}',
                                      location        = vhex,
                                      useGridLocation = True,
                                      owningBoard     = main['mapName']).\
                                      addPiece(piece)
                traits = cpy.getTraits()
                basic  = traits.pop()
                traits.append(MarkTrait(name='VP',value=vp))
                cpy.setTraits(*traits,basic)
            
            
        elif faction in ['de ','ss ','us ']:
            traits  = piece.getTraits()
            basic   = traits.pop()

            pg      = Trait.findTrait(traits,PrototypeTrait.ID,
                                      key = 'name',
                                      value = 'armoured infantry prototype')
            if pg is not None:
                ap = Trait.findTrait(traits,PrototypeTrait.ID,
                                     key   = 'name',
                                     value = 'armoured prototype')
                ip = Trait.findTrait(traits,PrototypeTrait.ID,
                                     key   = 'name',
                                     value = 'infantry prototype')
                if ap is not None: traits.remove(ap)
                if ip is not None: traits.remove(ip)

            
            hx = None
            dv = None
            for trait in traits:
                if trait.ID == MarkTrait.ID:
                    if hx is None and trait['name'] == 'upper left':
                        st = trait['value']
                        st = sub('[^=]+=','',st).strip()
                        hx = st
                        if hx == '':
                            hx = None
                    if trait['name'] == 'right':
                        st  = trait['value']
                        st2 = sub('[^=]+=','',st).strip()
                        dv  = st2
                        print(f'{name:20s}: {dv}')
                        if dv == '':
                            dv = None

            if dv is not None:
                traits.append(MarkTrait(name='Division',value=dv))

            traits.append(basic)
            piece.setTraits(*traits)

            if hx is not None and len(hx) == 4 and Faction == 'DE':
                vhex = hx[:2]+'@'+hx[2:]
                cpy  = main.addAtStart(name            = f'{name}@{vhex}',
                                       location        = vhex,
                                       useGridLocation = True,
                                       owningBoard     = main['mapName']).\
                                       addPiece(piece)

            

#
# EOF
#

                              
